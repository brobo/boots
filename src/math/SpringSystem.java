/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package math;

public abstract class SpringSystem {
	
	public Context c;
	public SpringSystem(Context c) {
		this.c = c;
	}
	
	public abstract double timeToStop();
	public abstract double maxAcceleration();
	public abstract double bootLength();
	
}
