/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package math;
public class Context {
	// b -- resistive force on spring
	// k -- spring constant
	// m -- mass of load
	final double m, b, k;
	
	// v_term -- terminal velocity
	// a_max -- maximum acceleration
	final double v_term, a_max;
	
	final double g = 9.81;
	
	public final double omega_0, omega_1, beta, delta;
	
	public Context(double m, double b, double k, double v_term, double a_max) {
		this.m = m;
		this.b = b;
		this.k = k;
		this.v_term = v_term;
		this.a_max = a_max;
		
		omega_0 = Math.sqrt(k / m);
		beta = b / m;
		if (omega_0 > beta) {
			omega_1 = Math.sqrt(omega_0 * omega_0 - beta * beta);
		} else {
			omega_1 = Math.sqrt(beta * beta - omega_0 * omega_0);
		}
		delta = Math.atan(beta / omega_1 + (v_term * omega_0 * omega_0) / (g * omega_1));
	}
	
	public SpringSystem getSystem() {
		if (omega_0 > beta) {
			return new UnderdampedSystem(this);
		} else {
			return new OverdampedSystem(this);
		}
	}
	
	public Context setM(double m) {
		return new Context(m, b, k, v_term, a_max);
	}
	
	public Context setB(double b) {
		return new Context(m, b, k, v_term, a_max);
	}
	
	public Context setK(double k) {
		return new Context(m, b, k, v_term, a_max);
	}
	
	public Context setV(double v_term) {
		return new Context(m, b, k, v_term, a_max);
	}
	
	public Context setA(double a_max) {
		return new Context(m, b, k, v_term, a_max);
	}
}
