/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package math;

public class OverdampedSystem extends SpringSystem {
	
	final double c1, c2;

	public OverdampedSystem(Context c) {
		super(c);
		c1 = (1 / (2 * c.omega_1)) * 
				(c.v_term + (c.g / (c.omega_0 * c.omega_0)) * (c.beta + c.omega_1));
		
		c2 = (c.g / (c.omega_0 * c.omega_0)) - c1; 
	}

	@Override
	public double timeToStop() {
		return (1 / (2 * c.omega_1))
				* Math.log(-(c2 * (-c.beta - c.omega_1)) / (c1 * (-c.beta + c.omega_1)));  
	}
	
	public double acceleration(double t) {
		return c1 * Math.pow(-c.beta + c.omega_1, 2) * Math.exp((-c.beta + c.omega_1) * t)
				+ c2 * Math.pow(-c.beta - c.omega_1, 2) * Math.exp((-c.beta - c.omega_1) * t);
	}

	@Override
	public double maxAcceleration() {
		double t = (1 / (2 * c.omega_1 * c.omega_1))
				* Math.log(-(c2 * Math.pow(-c.beta - c.omega_1, 3)) 
						/ (c1 * Math.pow(-c.beta + c.omega_1, 3)));
		
//		System.out.println(t);
		
		return Math.min(acceleration(0), acceleration(t));
	}

	@Override
	public double bootLength() {
		double t = timeToStop();
		
		return c1 * Math.exp((-c.beta + c.omega_1) * t) 
				+ c2 * Math.exp((-c.beta - c.omega_1) * t)
				- c.g / (c.omega_0 * c.omega_0);
	}

}
