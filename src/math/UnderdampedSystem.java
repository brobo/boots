/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package math;
public class UnderdampedSystem extends SpringSystem {
	
	public UnderdampedSystem(Context c) {
		super(c);
	}

	@Override
	public double timeToStop() {
		return (1 / c.omega_1) * (Math.atan(-c.beta / c.omega_1) + c.delta);
	}
	
	public double acceleration(double t) {
		return (c.g / (c.omega_0 * c.omega_0 * Math.cos(c.delta))) 
				* Math.exp(-c.beta * t)
				* ((c.beta * c.beta - c.omega_1 * c.omega_1)* Math.cos(c.omega_1 * t - c.delta)
						+ 2 * c.beta * c.omega_1 * Math.sin(c.omega_1 * t - c.delta)
						);
	}

	@Override
	public double maxAcceleration() {
		double param = (3 * c.beta - c.omega_1 * c.omega_1) / (3 * c.omega_1 - c.beta * c.beta);
		double t = (1 / c.omega_1) * (Math.atan(param) + c.delta);
//		System.out.println(t);
		return Math.min(acceleration(0), acceleration(t));
		
		
	}

	@Override
	public double bootLength() {
		double t = timeToStop();
		
		return (c.g / (c.omega_0 * c.omega_0 * Math.cos(c.delta)))
				* (Math.exp(-c.beta * t))
				* Math.cos(Math.atan(c.beta / c.omega_1))
				- (c.g / (c.omega_0 * c.omega_0));
	}
}
