/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package gui;

import java.util.function.Function;

import math.Context;

public enum Independent {
	MASS("Mass", (Context c) -> c::setM),
	RESISTIVE("Resistive Force", (Context c) -> c::setB),
	SPRING("Spring Constant", (Context c) -> c::setK),
	TERMINAL("Terminal Velocity", (Context c) -> c::setV);
	
	public String text;
	public Function<Context, Function<Double, Context>> f;
	
	private Independent(String text, Function<Context, Function<Double, Context>> f) {
		this.text = text;
		this.f = f;
	}
	
	public String toString() {
		return text;
	}
}
