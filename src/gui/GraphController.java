/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package gui;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import math.Context;

public class GraphController {
	
	@FXML
	LineChart<String, Double> lineChart;
	
	@FXML
	ComboBox<Dependent> comboBox;
	
	@FXML
	Button graphButton;
	
	@FXML
	Slider springSlider;
	@FXML
	Slider resistiveSlider;
	@FXML
	Slider terminalSlider;
	@FXML
	Slider massSlider;
	
	@FXML
	RadioButton springRadio;
	@FXML
	RadioButton resistiveRadio;
	@FXML
	RadioButton terminalRadio;
	@FXML
	RadioButton massRadio;
	
	@FXML
	Label springLabel;
	@FXML
	Label resistiveLabel;
	@FXML
	Label terminalLabel;
	@FXML
	Label massLabel;

	ToggleGroup group = new ToggleGroup();
	
	VariableSlider springVariable;
	VariableSlider resistiveVariable;
	VariableSlider terminalVariable;
	VariableSlider gVariable;
	
	public void init() {
		springVariable = new VariableSlider(springSlider, springLabel, springRadio);
		resistiveVariable = new VariableSlider(resistiveSlider, resistiveLabel, resistiveRadio);
		terminalVariable = new VariableSlider(terminalSlider, terminalLabel, terminalRadio);
		gVariable = new VariableSlider(massSlider, massLabel, massRadio);
		
		springRadio.setUserData(Independent.SPRING);
		springRadio.setToggleGroup(group);
		resistiveRadio.setUserData(Independent.RESISTIVE);
		resistiveRadio.setToggleGroup(group);
		terminalRadio.setUserData(Independent.TERMINAL);
		terminalRadio.setToggleGroup(group);
		massRadio.setUserData(Independent.MASS);
		massRadio.setToggleGroup(group);
		
		massSlider.setValue(72.0);
		terminalSlider.setValue(56.0);
		resistiveSlider.setValue(5.0);
		springSlider.setValue(14.0);
		
		comboBox.getItems().addAll(Dependent.values());
		comboBox.setValue(Dependent.BOOT_LENGTH);
	}
	
	public void graph() {
		Context baseContext = new Context(massSlider.getValue(),
				resistiveSlider.getValue(),
				springSlider.getValue(),
				terminalSlider.getValue(),
				0.0);
		Independent independent = (Independent)group.getSelectedToggle().getUserData();
		Dependent dependent = (Dependent)comboBox.getValue();
		XYChart.Series<String,Double> series = new XYChart.Series<>();
		for (int i = 0; i <= 100; i++) {
			Context c = independent.f.apply(baseContext).apply((double)i);
			double x = (double)i;
			double y = dependent.f.apply(c.getSystem()).get();
//			System.out.printf("%.2f %.2f %s %.2f%n", x, y, (c.getSystem() instanceof UnderdampedSystem) ? "under" : "over", c.omega_1);
			if (!Double.isNaN(x) && !Double.isNaN(y)) {
				series.getData().add(new XYChart.Data<String,Double>(""+x, y));
			}
		}
//		System.out.println();
		lineChart.getData().clear();
		lineChart.getData().add(series);
	}
}
