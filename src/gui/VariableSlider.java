/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package gui;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;

public class VariableSlider {
	
	Slider slider;
	Label label;
	RadioButton radio;
	
	public VariableSlider(Slider slider, Label label, RadioButton radio) {
		this.slider = slider;
		this.label = label;
		this.radio = radio;
		
		this.slider.valueProperty().addListener(
				(ObservableValue<? extends Number> o, Number old, Number newValue) -> {
					label.setText(String.format("%.2f", newValue.doubleValue()));
				});
	}
	
}
