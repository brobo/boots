/*
 * @author Colby Brown
 * Copyright (c) 2016
 * This software released under the MIT license. See license.txt for more info.
 */
package gui;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import math.SpringSystem;

public enum Dependent {
	TIME_TO_FALL("Time to fall", (SpringSystem s) -> () -> s.timeToStop()),
	MAXIMUM_ACCELERATION("Maximum acceleration", (SpringSystem s) -> () -> s.maxAcceleration()),
	BOOT_LENGTH("Boot length", (SpringSystem s) -> () -> s.bootLength()),
	/*OMEGA_0("Omega 0", (SpringSystem s) -> () -> s.c.omega_0),
	DELTA("Delta", (SpringSystem s) -> () -> s.c.delta),
	BETA("Beta", (SpringSystem s) -> () -> s.c.beta)*/;
	
	public String text;
	public Function<SpringSystem, Supplier<Double>> f;
	public Function<SpringSystem, Consumer<Double>> g;
	private Dependent(String text, Function<SpringSystem, Supplier<Double>> f) {
		this.text = text;
		this.f = f;
	}
	
	public String toString() {
		return text;
	}
}
