Boots grapher, created for extra credit for Dr. Manne's Physics 321 course at the University of Arizona.

Copyright (c) 2016 Colby Brown
This software is released under the MIT license. See license.txt for more information.

Use: Launch the boots.jar file with the command "java -jar boots.jar". Requires Java 8 or higher to run. 
Select a dependent variable from the drop down menu, and select an independent variable with the radio buttons. 
The value of the sliders for the other 3 variables will be taken as constants.

Problem description: Design a pair of boots such that a person could fall from any hight and survive. 
This application graphs key values against each other to help analyze the relationships between, for example, the terminal velocity of the person falling and the maximum acceleration they will experience, or the spring constant of the boots and the length of boot necessary.

For questions, comments, or concerns, email Colby Brown at colbybrown@email.arizona.edu.

